// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.goalkeeper.domain;

import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.GKUserDataOnDemand;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.stereotype.Component;

privileged aspect GKUserDataOnDemand_Roo_DataOnDemand {
    
    declare @type: GKUserDataOnDemand: @Component;
    
    private Random GKUserDataOnDemand.rnd = new SecureRandom();
    
    private List<GKUser> GKUserDataOnDemand.data;
    
    public GKUser GKUserDataOnDemand.getNewTransientGKUser(int index) {
        GKUser obj = new GKUser();
        setEmail(obj, index);
        setGKRole(obj, index);
        setPassword(obj, index);
        setTwitter(obj, index);
        setUsername(obj, index);
        return obj;
    }
    
    public void GKUserDataOnDemand.setEmail(GKUser obj, int index) {
        String Email = "foo" + index + "@bar.com";
        obj.setEmail(Email);
    }
    
    public void GKUserDataOnDemand.setGKRole(GKUser obj, int index) {
        String GKRole = "GKRole_" + index;
        obj.setGKRole(GKRole);
    }
    
    public void GKUserDataOnDemand.setPassword(GKUser obj, int index) {
        String password = "password_" + index;
        obj.setPassword(password);
    }
    
    public void GKUserDataOnDemand.setTwitter(GKUser obj, int index) {
        String twitter = "twitter_" + index;
        obj.setTwitter(twitter);
    }
    
    public void GKUserDataOnDemand.setUsername(GKUser obj, int index) {
        String username = "username_" + index;
        obj.setUsername(username);
    }
    
    public GKUser GKUserDataOnDemand.getSpecificGKUser(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        GKUser obj = data.get(index);
        Long id = obj.getId();
        return GKUser.findGKUser(id);
    }
    
    public GKUser GKUserDataOnDemand.getRandomGKUser() {
        init();
        GKUser obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return GKUser.findGKUser(id);
    }
    
    public boolean GKUserDataOnDemand.modifyGKUser(GKUser obj) {
        return false;
    }
    
    public void GKUserDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = GKUser.findGKUserEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'GKUser' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<GKUser>();
        for (int i = 0; i < 10; i++) {
            GKUser obj = getNewTransientGKUser(i);
            try {
                obj.persist();
            } catch (final ConstraintViolationException e) {
                final StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    final ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getRootBean().getClass().getName()).append(".").append(cv.getPropertyPath()).append(": ").append(cv.getMessage()).append(" (invalid value = ").append(cv.getInvalidValue()).append(")").append("]");
                }
                throw new IllegalStateException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
