package com.goalkeeper.domain;
import junit.framework.Assert;

import org.junit.Test;
import org.springframework.roo.addon.test.RooIntegrationTest;
import org.springframework.transaction.annotation.Transactional;

@RooIntegrationTest(entity = GKUser.class)
public class GKUserIntegrationTest {

    @Test
    public void testMarkerMethod() {
    }
    
    @Test
    @Transactional
    public void testfindUserByUsernameAndPassword(){
    	GKUser validUser = new GKUser();
    	String validUsername = "xyztestobject", validPassword = "abcdef", invalidUsername = "abctestobject", invalidPassword="zzz";
    	
    	validUser.setEmail("xyz@valid.com");
    	
    	validUser.setUsername(validUsername);
    	validUser.setPassword(validPassword);
    	validUser.persist();
    	validUser.flush();
    	
    	
    	Assert.assertNotNull(GKUser.findGKUSerByUsernameAndPasswordEquals(validUsername, validPassword));
    	
    	// These should all fail to find a user
    	Assert.assertNull(GKUser.findGKUSerByUsernameAndPasswordEquals(validUsername, invalidPassword));
    	Assert.assertNull(GKUser.findGKUSerByUsernameAndPasswordEquals(invalidUsername, validPassword));
    	Assert.assertNull(GKUser.findGKUSerByUsernameAndPasswordEquals(invalidUsername, invalidPassword));
    	
    }
    
    @Test
    @Transactional
    public void testFindUserByName() {
    	GKUser existingUser = new GKUser();
    	GKUser unpersistedUser = new GKUser();
    	String realname = "real", unpersisted = "ghost", neverexist = "imaginary";
    	
    	existingUser.setUsername(realname);
    	existingUser.setPassword("a");
    	existingUser.setEmail("a@a.com");
    	existingUser.persist();
    	existingUser.flush();
    	
    	unpersistedUser.setUsername(unpersisted);
    	unpersistedUser.setPassword("g");
    	existingUser.setEmail("g@g.com");
    	
    	Assert.assertNotNull(GKUser.findUserByName(realname));
    	Assert.assertNull(GKUser.findUserByName(unpersisted));
    	Assert.assertNull(GKUser.findUserByName(neverexist));
    	
    	
    }
}
