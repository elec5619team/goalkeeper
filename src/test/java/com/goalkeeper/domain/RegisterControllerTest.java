package com.goalkeeper.domain;

import static org.junit.Assert.*;

import java.util.List;

//import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;

import com.goalkeeper.web.DriveFile;
import com.google.api.client.util.DateTime;

public class RegisterControllerTest 
{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		/*
		
		//USE MOCK data
		String name = "Requirements Doc";
		String viewLink = "https://docs.google.com/document/d/135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc/edit?usp=drivesdk";
		String downloadLink = "https://docs.google.com/feeds/download/documents/export/Export?id=135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc&exportFormat=pdf";
		String fileID = "135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc";
		String lastUserToModify="David Farias";
		
		//String fileID,String name, String viewLink, String downloadLink
		DriveFile tdf = new DriveFile(fileID,name,viewLink,downloadLink);
		tdf.setLastUserToModify(lastUserToModify);
		*/
	}

/*

	@Test
	public void testRegister() {
		fail("Not yet implemented");
	}


	@Test
	public void testInsertModel() {
		fail("Not yet implemented");
	}

	@Test
	public void testInsertHttpServletRequest() {
		fail("Not yet implemented");
	}

	@Test
	public void testListing() 
	{
		Assert.
	}
*/
	@Test
	public void testAnalytics() 
	{
		//USE MOCK data
				String name = "Requirements Doc";
				String viewLink = "https://docs.google.com/document/d/135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc/edit?usp=drivesdk";
				String downloadLink = "https://docs.google.com/feeds/download/documents/export/Export?id=135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc&exportFormat=pdf";
				String fileID = "135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc";
				String lastUserToModify="David Farias";
				
				//String fileID,String name, String viewLink, String downloadLink
				DriveFile tdf = new DriveFile(fileID,name,viewLink,downloadLink);
				tdf.setLastUserToModify(lastUserToModify);	

		Assert.assertEquals(tdf.getName(),"Requirements Doc");
		Assert.assertEquals(tdf.getViewLink(),"https://docs.google.com/document/d/135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc/edit?usp=drivesdk");
		Assert.assertEquals(tdf.getDownloadLink(),"https://docs.google.com/feeds/download/documents/export/Export?id=135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc&exportFormat=pdf");
		Assert.assertEquals(tdf.getFileID(),"135TKa74boe69RaG9mUu4QQCgW2QXsVF_I8dRO3EFGNc");
		Assert.assertEquals(tdf.getLastUserToModify(),"David Farias");
		
		//fail("Not yet implemented");
	}


}
