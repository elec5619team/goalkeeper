package com.goalkeeper.domain;

import junit.framework.TestCase;

public class TaskTests extends TestCase{
	private Task task;
	
	protected void setUp() throws Exception {
		task = new Task();
	}
	
	
	
	public void testSetTaskname(){
		String testTaskname = "test name";
		assertNull(task.getTaskname());
		task.setTaskname(testTaskname);
		assertEquals(testTaskname, task.getTaskname());
	}

}
