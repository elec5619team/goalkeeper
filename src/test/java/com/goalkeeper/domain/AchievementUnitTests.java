package com.goalkeeper.domain;

import junit.framework.TestCase;

public class AchievementUnitTests extends TestCase{
	private Achievement achievement;
	
	protected void setUp() throws Exception {
		achievement = new Achievement();
	}
	
	public void testSetAchievementDescription(){
		String testDescription = "have 1 task";
		assertNull(achievement.getDescription());
		achievement.setDescription(testDescription);
		assertEquals(testDescription, achievement.getDescription());
	}
	
	public void testSetAchievementName(){
		String testName = "Task Master";
		assertNull(achievement.getName());
		achievement.setName(testName);
		assertEquals(testName, achievement.getName());
	}
	
	public void testSetAchievementTimesAwarded(){
		Integer testTimesAwarded = 4;
		assertNull(achievement.getTimesAwarded());
		achievement.setTimesAwarded(testTimesAwarded);
		assertEquals(testTimesAwarded, achievement.getTimesAwarded());
	}

}
