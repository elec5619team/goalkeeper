package com.goalkeeper.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
//import org.springframework.mock.staticmock.MockStaticEntityMethods;

import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;

import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.Notification;
import com.goalkeeper.domain.Task;

@RunWith(JUnit4.class)
//@MockStaticEntityMethods

public class notifyMailTest extends notifyMail {

	@Autowired public MailSender mailSender;
    public List<Notification> notifications;
	public static GKUser u;
	public static Task t;
    
    @BeforeClass
    static public void startSetUp() throws Exception {
		//Create User
    	u = new GKUser();
    	u.setUsername("TestUser");
    	u.setEmail("test@test.com");
    	u.setPassword("TestPassword");
    	u.persist();
    	//Create Task
    	t = new Task();
    	t.setTaskname("TestTask");
    	t.persist();
    }
    
	@Before
	public void setUp() throws Exception {
		//Clear Notifications
    	notifications = Notification.findAllNotifications();
    	if (notifications != null){
    		for (ListIterator<Notification> iter = notifications.listIterator(); iter.hasNext(); ) {
    			Notification notification = iter.next();
    			notification.remove();
    		}
    	}
    	//Create Notifcation
    	Notification n = new Notification();
    	n.setCompletetion(false);
    	n.setEmail(true);
    	n.setGKUser(u);
    	n.setName("TestNotification");
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.MINUTE, 15);
    	n.setNotificationTime(cal.getTime());
    	n.setRepeats(0);
    	n.setRepeatTime(0);
    	n.setReponseTime(null);
    	n.setTask(t);
    	n.setTwitter(false);
    	n.persist();
    	System.out.println("@Before");
	}
	
	//Test that the mailSender is not a null value
	@Test
	public void mailSenderNull() throws Exception {
		run();
		Assert.assertNotNull(mailSender);
	}
	
	//Test that there is no notification Returns success
	@Test
	public void noNotifications() throws Exception {
		//Clear Notification
    	notifications = Notification.findAllNotifications();
    	for (ListIterator<Notification> iter = notifications.listIterator(); iter.hasNext(); ) {
    		Notification notification = iter.next();
    		notification.remove();
    	}
    	try {
    		run();
    		Assert.assertTrue(true);
    	}
    	catch (Exception e) {
    		fail(e.toString());
    	}
	}
	
	//Notification that should not send. 
	@Test
	public void noSendNotification() throws Exception {
		run();
    	notifications = Notification.findAllNotifications();
		Assert.assertFalse(notifications.get(0).getCompletetion());
	}
	
	//Notification that should send.
	@Test
	public void sendNotification() throws Exception {
    	notifications = Notification.findAllNotifications();
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.HOUR , -1);
    	notifications.get(0).setNotificationTime(cal.getTime());
    	notifications.get(0).merge();
    	run();
    	notifications = Notification.findAllNotifications();
		Assert.assertTrue(notifications.get(0).getCompletetion());
	}
	
	//Send a notification twice using repeats
	@Test
	public void repeats() throws Exception {
		notifications = Notification.findAllNotifications();
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.HOUR , -1);
    	notifications.get(0).setNotificationTime(cal.getTime());
    	notifications.get(0).setRepeats(1);
    	notifications.get(0).setRepeatTime(1);
    	notifications.get(0).merge();
    	run();
    	notifications = Notification.findAllNotifications();
		Assert.assertFalse(notifications.get(0).getCompletetion());
		Assert.assertEquals((int)(notifications.get(0).getRepeats()), 0);
		run();
    	notifications = Notification.findAllNotifications();
		Assert.assertTrue(notifications.get(0).getCompletetion());
		Assert.assertEquals((int)(notifications.get(0).getRepeats()), 0);
	}
	
	//@Test
	public void manyNoSendNotifcation() throws Exception {
		notifications = Notification.findAllNotifications();
		Notification n = notifications.get(0);
		for (int i = 1; i <= 100; i++ ){
			notifications.add(n);
			notifications.get(i).persist();
		}
		run();
    	notifications = Notification.findAllNotifications();
    	for (ListIterator<Notification> iter = notifications.listIterator(); iter.hasNext(); ) {
    		Notification notification = iter.next();
    		Assert.assertFalse(notification.getCompletetion());

    	}
	}
	
	//@Test
	public void manySendNotifcation() throws Exception {
		notifications = Notification.findAllNotifications();
    	Calendar cal = Calendar.getInstance();
    	cal.add(Calendar.HOUR , -1);
    	notifications.get(0).setNotificationTime(cal.getTime());
    	notifications.get(0).merge();
		Notification n = notifications.get(0);
		for (int i = 1; i <= 100; i++ ){
			notifications.add(n);
			notifications.get(i).persist();
		}
		run();
    	notifications = Notification.findAllNotifications();
    	for (ListIterator<Notification> iter = notifications.listIterator(); iter.hasNext(); ) {
    		Notification notification = iter.next();
    		Assert.assertTrue(notification.getCompletetion());
    	}
	}
	
	
	 
}
