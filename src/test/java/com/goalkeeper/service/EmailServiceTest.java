package com.goalkeeper.service;

import static org.junit.Assert.fail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.junit.Test;

public class EmailServiceTest extends EmailService {

	@Autowired public MailSender mailSender;
	
	//sendMessage(String mailFrom, String subject, String mailTo, String message, MailSender mailSender )

	//Test with null entries
	@Test
	public void nullentries(){
		try {
			sendMessage(null,null,null,null,null);
		}
		catch (Exception e) {
			fail(e.toString());
		}
	}
	
	//Test with valid entries
	@Test
	public void valid(){
		try {
			sendMessage("gknotfiy@gmail.com", "TestSubject", "Test@email.com","TestMEssage",mailSender);
		}
		catch (Exception e) {
			fail(e.toString());
		}
	}
	
}
