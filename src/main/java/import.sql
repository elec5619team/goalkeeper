
INSERT INTO achievement (id, description, name, times_awarded, version) VALUES (nextval('hibernate_sequence'),'Have 1 task','First Steps',0, 0 );
INSERT INTO achievement (id, description, name, times_awarded, version) VALUES (nextval('hibernate_sequence'),'Have 10 all-time tasks','Experienced Taskster',0, 0 );
INSERT INTO achievement (id, description, name, times_awarded, version) VALUES (nextval('hibernate_sequence'),'Have 25 all-time tasks','Task Master',0, 0 );
INSERT INTO achievement (id, description, name, times_awarded, version) VALUES (nextval('hibernate_sequence'),'Have 2 concurrent unfinished tasks','Multitasker',0, 0 );
INSERT INTO achievement (id, description, name, times_awarded, version) VALUES (nextval('hibernate_sequence'),'Be in a group','Collaborator',0, 0 );

INSERT INTO gkuser (version, id, username, password, email) VALUES (0,nextval('hibernate_sequence'), 'TestTurtle', 'abcd', 'turtle@xyz.com');
INSERT INTO gkuser (version, id, username, password, email) VALUES (0,nextval('hibernate_sequence'), 'TestTortoise', 'abcd', 'turtle@xyz.com');
INSERT INTO gkuser (version, id, username, password, email, GKRole) VALUES (0,nextval('hibernate_sequence'), 'tf', 'tf', 'tf@xyz.com', 'user'); -- Test flamingo (nonadmin)
INSERT INTO gkuser (version, id, username, password, email, GKRole) VALUES (0,nextval('hibernate_sequence'), 'john', 'john', 'john@xyz.com', 'admin'); -- Test john (admin)
INSERT INTO gkuser (version, id, username, password, email, GKRole) VALUES (0,nextval('hibernate_sequence'), 'shane', 'shane', 'shane@xyz.com', 'user'); -- non admin user
INSERT INTO gkuser (version, id, username, password, email) VALUES (0,nextval('hibernate_sequence'), 'TestOstrich', 'abcd', 'ostrich@xyz.com');

-- Create some user groups
INSERT INTO user_group (version, id, name) VALUES (0,nextval('hibernate_sequence'), 'Bird Group');
INSERT INTO user_group (version, id, name) VALUES (0,nextval('hibernate_sequence'), 'Chill Out');

-- Join TestFlamingo to some user groups
INSERT INTO user_group_users (groups, users) SELECT ug.id, gk.id FROM gkuser gk, user_group ug WHERE gk.username = 'tf' AND ug.name = 'Bird Group';
INSERT INTO user_group_users (groups, users) SELECT ug.id, gk.id FROM gkuser gk, user_group ug WHERE gk.username = 'TestOstrich' AND ug.name = 'Bird Group';

-- Other groups that TestFlamingo is not part of
INSERT INTO user_group_users (groups, users) SELECT ug.id, gk.id FROM gkuser gk, user_group ug WHERE gk.username = 'TestTurtle' AND ug.name = 'Chill Out';

-- Create some miletones and connect it to a group
INSERT INTO milestone (version, id, milestonename, usergroup) SELECT 0, nextval('hibernate_sequence'), 'Test Empty Milestone', ug.id FROM user_group ug WHERE ug.name = 'Bird Group';
INSERT INTO milestone (version, id, milestonename, usergroup) SELECT 0, nextval('hibernate_sequence'), 'Test All Finished', ug.id FROM user_group ug WHERE ug.name = 'Bird Group';
INSERT INTO milestone (version, id, milestonename, usergroup) SELECT 0, nextval('hibernate_sequence'), 'Test All Incomplete', ug.id FROM user_group ug WHERE ug.name = 'Bird Group';
INSERT INTO milestone (version, id, milestonename, usergroup) SELECT 0, nextval('hibernate_sequence'), 'Test Partial', ug.id FROM user_group ug WHERE ug.name = 'Bird Group';
INSERT INTO milestone (version, id, milestonename, usergroup) SELECT 0, nextval('hibernate_sequence'), 'Flamingo can''t see this!', ug.id FROM user_group ug WHERE ug.name = 'Chill Out';


-- Create some test tasks for milestones

-- All finished
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '1 hour', 100, m.id, gk.id, 'Complete Task1' FROM milestone m, GKUser gk WHERE m.milestonename='Test All Finished' and gk.username='tf';
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '2 hour', 100, m.id, gk.id, 'Complete Task2' FROM milestone m, GKUser gk WHERE m.milestonename='Test All Finished' and gk.username='tf';
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '3 hour', 100, m.id, gk.id, 'Complete Task3' FROM milestone m, GKUser gk WHERE m.milestonename='Test All Finished' and gk.username='tf';

-- All unfinished
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '4 hour', 0, m.id, gk.id, 'Incomplete Task1' FROM milestone m, GKUser gk WHERE m.milestonename='Test All Incomplete' and gk.username='tf';
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '5 hour', 0, m.id, gk.id, 'Incomplete Task2' FROM milestone m, GKUser gk WHERE m.milestonename='Test All Incomplete' and gk.username='tf';
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '6 hour', 0, m.id, gk.id, 'Incomplete Task3' FROM milestone m, GKUser gk WHERE m.milestonename='Test All Incomplete' and gk.username='tf';

-- Partial
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '7 hour', 100, m.id, gk.id, 'Partial CompleteTask1' FROM milestone m, GKUser gk WHERE m.milestonename='Test Partial' and gk.username='tf';
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '8 hour', 100, m.id, gk.id, 'Partial CompleteTask2' FROM milestone m, GKUser gk WHERE m.milestonename='Test Partial' and gk.username='tf';
INSERT INTO task (version, id, createdon, progress, milestone, owner, taskname) SELECT 0, nextval('hibernate_sequence'), now() + interval '9 hour', 0, m.id, gk.id, 'Partial IncompleteTask1' FROM milestone m, GKUser gk WHERE m.milestonename='Test Partial' and gk.username='tf';

--Create some dummy tasks
INSERT INTO task (version, id, taskname, owner) SELECT 0, nextval('hibernate_sequence'), 'Sample Task 1',gk.id FROM gkuser gk WHERE gk.username = 'tf';

