package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.Size;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class IssueComment {

    /**
     */
    @DateTimeFormat(style = "M-")
    private Date createdon;

    /**
     */
    @Size(min = 1)
    private String comment;

    /**
     */
    @NotNull
    @ManyToOne
    private Issue issue;

    /**
     */
    @NotNull
    @ManyToOne
    private GKUser gkuser;
}
