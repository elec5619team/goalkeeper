// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.goalkeeper.domain;

import com.goalkeeper.domain.Milestone;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect Milestone_Roo_Configurable {
    
    declare @type: Milestone: @Configurable;
    
}
