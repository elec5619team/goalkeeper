package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class UserGroup {

    /**
     */
    @NotNull
    @Column(unique = true)
    @Size(min = 3)
    private String name;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<GKUser> users = new HashSet<GKUser>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "usergroup")
    private Set<UpdateMessage> updates = new HashSet<UpdateMessage>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "usergroup")
    private Set<Milestone> milestones = new HashSet<Milestone>();
    
    public Set<GKUser> getUsers(){
    	return users;
    }
    
    // Custom methods
    public static boolean isUserMemberOfGroup(GKUser user, UserGroup ug) {
        return ug.getUsers().contains(user);
    }
    
    public void nonTransactionalPersist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
}
