// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.goalkeeper.domain;

import com.goalkeeper.domain.Issue;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect Issue_Roo_Jpa_Entity {
    
    declare @type: Issue: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Issue.id;
    
    @Version
    @Column(name = "version")
    private Integer Issue.version;
    
    public Long Issue.getId() {
        return this.id;
    }
    
    public void Issue.setId(Long id) {
        this.id = id;
    }
    
    public Integer Issue.getVersion() {
        return this.version;
    }
    
    public void Issue.setVersion(Integer version) {
        this.version = version;
    }
    
}
