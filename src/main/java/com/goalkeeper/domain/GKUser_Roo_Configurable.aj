// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.goalkeeper.domain;

import com.goalkeeper.domain.GKUser;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect GKUser_Roo_Configurable {
    
    declare @type: GKUser: @Configurable;
    
}
