// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.goalkeeper.domain;

import com.goalkeeper.domain.UpdateMessage;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect UpdateMessage_Roo_Jpa_Entity {
    
    declare @type: UpdateMessage: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long UpdateMessage.id;
    
    @Version
    @Column(name = "version")
    private Integer UpdateMessage.version;
    
    public Long UpdateMessage.getId() {
        return this.id;
    }
    
    public void UpdateMessage.setId(Long id) {
        this.id = id;
    }
    
    public Integer UpdateMessage.getVersion() {
        return this.version;
    }
    
    public void UpdateMessage.setVersion(Integer version) {
        this.version = version;
    }
    
}
