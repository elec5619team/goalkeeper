package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Milestone {

    /**
     */
    @NotNull
    @Size(min = 1)
    private String milestonename;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "milestone")
    private Set<Issue> issues = new HashSet<Issue>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "milestone")
    private Set<Task> tasks = new HashSet<Task>();

    /**
     */
    //@NotNull
    @ManyToOne
    private UserGroup usergroup;
    
    /* Custom methods */
    
    // Because eclipse gives errors otherwise <_<
    public UserGroup getUsergroup() {
        return this.usergroup;
    }
    
    public static boolean isOwnerOfMilestone(GKUser user, Milestone m) {
    	// Get usergroup which owns milestone
    	UserGroup ug = m.getUsergroup();
    	Long uid = user.getId();
    	
    	// For now just do this the manual way
    	for (GKUser gku : ug.getUsers())
    	{
    		if (gku.getId().equals(uid)){
    			return true;
    		}
    	}
    	return false;
        //return entityManager().createQuery("FROM UserGroup AS ug LEFT JOIN GKUser AS gkuser WHERE username = :username", GKUser.class).setParameter("username", username).getSingleResult();
    }
       
}
