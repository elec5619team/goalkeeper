package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.persistence.ManyToOne;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Notification {

    /**
     */
    @NotNull
    @Size(min = 3)
    private String name;

    /**
     */
    @ManyToOne
    @NotNull
    private GKUser GKUser;

    /**
     */
    @ManyToOne
    @NotNull
    private Task Task;

    /**
     */
    @NotNull
    @Column(name="ts", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @DateTimeFormat(style = "MS")
    private Date NotificationTime;

    /**
     */
    @Value("0")
    @Min(0L)
    private Integer Repeats;

    /**
     */
    @Value("15")
    @Min(0L)
    private Integer RepeatTime;

    /**
     */
    @Value("false")
    private Boolean twitter;
    
    @Value("true")
    private Boolean email;

    /**
     */
    @Value("false")
    private Boolean Responded;

    /**
     */
    @DateTimeFormat(style = "-S")
    private Date reponseTime;

    /**
     */
    @Value("false")
    private Boolean Completetion;


}
