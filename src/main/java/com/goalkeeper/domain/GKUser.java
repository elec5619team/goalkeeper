package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.validator.constraints.Email;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class GKUser {

    /**
     */
    @NotNull
    @Column(unique = true)
    @Size(min = 3)
    private String username;

    /**
     */
    @NotNull
    @Size(min = 1)
    private String password;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    private Set<Task> tasks = new HashSet<Task>();

    /**
     */
    private String twitter;

    @Email
    @NotNull
    private String Email;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "users")
    private Set<UserGroup> groups = new HashSet<UserGroup>();

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "gkuser")
    private Set<IssueComment> comments = new HashSet<IssueComment>();

    /*
     *	Custom methods
     */
    public static GKUser findUserByName(String username) {
        List<GKUser> q = entityManager().createQuery("SELECT o FROM GKUser o WHERE username = :username", GKUser.class).setParameter("username", username).getResultList();
        if (q.size() == 0) {
            return null;
        } else {
            System.out.println("SIZE: " + q.size());
            return q.get(0);
        }
    }

    /*
    public static GKUser findGKUSerByUsernameAndPasswordEquals(String username, String password) {
        return entityManager().createQuery("SELECT o FROM GKUser o WHERE username = :username AND password = :password", GKUser.class).setParameter("username", username).setParameter("password", password).getSingleResult();
    }*/
    
    public static GKUser findGKUSerByUsernameAndPasswordEquals(String username, String password) {
        //return entityManager().createQuery("SELECT o FROM GKUser o WHERE username = :username AND password = :password", GKUser.class).setParameter("username", username).setParameter("password", password).getSingleResult();
    	List<GKUser> q = entityManager().createQuery("SELECT o FROM GKUser o WHERE username = :username AND password = :password", GKUser.class).setParameter("username", username).setParameter("password", password).getResultList();
        if (q.size() == 0) {
            return null;
        } else {
            System.out.println("SIZE: " + q.size());
            return q.get(0);
        }  	
    	
    }

    public static GKUser getLoggedInUser(Principal principal) {
        return GKUser.findUserByName(principal.getName());
    }

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    private Set<Issue> issues = new HashSet<Issue>();

    /**
     */
    private String GKRole;
}
