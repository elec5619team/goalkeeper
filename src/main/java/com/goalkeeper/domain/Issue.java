package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Size;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Issue {

    /**
     */
    //@NotNull
    @ManyToOne
    private Milestone milestone;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "issue")
    private Set<IssueComment> comments = new HashSet<IssueComment>();

    /**
     */
    //@Size(min = 1)
    private String content;

    /**
     */
    //@NotNull
    @ManyToOne
    private GKUser owner;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date dateraised;
}
