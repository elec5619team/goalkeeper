package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;


import javax.validation.constraints.NotNull;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Achievement {

    /**
     */
    @NotNull
    private String name;

    /**
     */
    private String description;

    /**
     */
    private Integer timesAwarded;

    /**
     */
    @ManyToMany(cascade = CascadeType.ALL)
    private Set<GKUser> ownedBy = new HashSet<GKUser>();
	public void awardAchievement(int user, Achievement achievement) {
		//add to user's achievement list
		//this.isOwned = true;
		this.timesAwarded += 1;
		return;
	}
    
}
