package com.goalkeeper.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Task {

    /**
     */
    @NotNull
    @Size(min = 3)
    private String taskname;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date createdon;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date duedate;

    /**
     */
    private Integer priority;

    /**
     */
    
    private Integer wordCount;

    /**
     */
    
    private Integer progress;

    /**
     */
    private Integer difficulty;

    /**
     */
    private Integer estimated_duration;

    /**
     */
    /*@NotNull*/
    @ManyToOne
    private GKUser owner;

    /**
     */
    @ManyToOne
    private Milestone milestone;
}
