// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.goalkeeper.domain;

import com.goalkeeper.domain.GKUser;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect GKUser_Roo_Jpa_Entity {
    
    declare @type: GKUser: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long GKUser.id;
    
    @Version
    @Column(name = "version")
    private Integer GKUser.version;
    
    public Long GKUser.getId() {
        return this.id;
    }
    
    public void GKUser.setId(Long id) {
        this.id = id;
    }
    
    public Integer GKUser.getVersion() {
        return this.version;
    }
    
    public void GKUser.setVersion(Integer version) {
        this.version = version;
    }
    
}
