package com.goalkeeper.service;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;

public class EmailService {
	
	
	

    public void sendMessage(String mailFrom, String subject, String mailTo, String message, MailSender mailSender ) {
        if (mailFrom == null || subject == null || mailTo == null || message == null) {
        	System.out.println("null entry in mail");
        	return;

        }
    	org.springframework.mail.SimpleMailMessage mailMessage = new org.springframework.mail.SimpleMailMessage();
        mailMessage.setFrom(mailFrom);
        mailMessage.setSubject(subject);
        mailMessage.setTo(mailTo);
        mailMessage.setText(message);
        try{
        	mailSender.send(mailMessage);
		}
		catch(MailException me){
			System.out.println("Cannot send email");			
			System.out.println(me);
		}	
		catch (Exception e) {
			System.out.println("Could not send email");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

    }
    
}
