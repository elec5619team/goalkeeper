package com.goalkeeper.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;

import com.goalkeeper.domain.Notification;

public class notifyMail extends TimerTask   {

	@Autowired
	private MailSender mailSender;
	private EmailService email;
    
    private List<Notification> notifications;
    
    public void run() {
		Date date = new Date();
    	System.out.println("Checking for notifcations at " + date);
    	notifications = Notification.findAllNotifications();
    	System.out.println("Notifications: " + notifications.size());
    	for (ListIterator<Notification> iter = notifications.listIterator(); iter.hasNext(); ) {
    		Notification notification = iter.next();
    		if (notification.getNotificationTime().before(date) && notification.getCompletetion().equals(false) ){
    			try{
    				System.out.println("Sending Notification " 
    						+ notification.getName() + " to " 
    						+ notification.getGKUser().getEmail());
    				email = new EmailService();
    				//Send the email 
    	    		email.sendMessage(
    	    				"gknotfiy@gmail.com",
    	    				"Notifcation",
    	    				notification.getGKUser().getEmail(),
    	    				"Notification " + notification.getName() + " for task " 
    	    				+ notification.getTask().getTaskname() + " at " 
    	    				+ notification.getNotificationTime().toString() 
    	    				+ "\n" + "Respond at: " 
    	    				+ "http://localhost:8080/goalkeeper/notifications/" 
    	    				+ notification.getId().toString()
    	    				+ "?form",
    	    				mailSender);
    	    		System.out.println("Notification Sent");
    	    		//Check for any repeats
    	    		if (notification.getRepeats() > 0){
    	    			notification.setRepeats(notification.getRepeats() - 1);
    	    	        Calendar cal = Calendar.getInstance();
    	    	        cal.setTime(date);
    	    	        cal.add(Calendar.MINUTE, notification.getRepeatTime());
    	    			notification.setNotificationTime(cal.getTime());
    	    		}
    	    		else {
    	    			notification.setCompletetion(true);
    	    		}
    	    		notification.merge();
    			}
    	    	catch (MailException ex ){
    	            System.err.println(ex.getMessage());  
    	            System.err.println(notification.getName() 
    	            + " Failed to send email to "
    	            + notification.getGKUser().getEmail());
    	    	}
    		}
    	}	

    }
    
    
}
