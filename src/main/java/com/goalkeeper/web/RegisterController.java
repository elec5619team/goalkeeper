package com.goalkeeper.web;
import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.Task;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.model.FileList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;


@Controller

public class RegisterController 
{
	
	private static String CLIENT_ID = "110473691586-9g49j155r4bjees7vcl32f49h747580i.apps.googleusercontent.com";
	private static String CLIENT_SECRET = "LfU1WRQktAhk5EG4HzV7dXOx";
	private static String REDIRECT_URI = "http://localhost:8080/goalkeeper/register2";

	private static HttpTransport httpTransport = new NetHttpTransport();
	private static JsonFactory jsonFactory = new JacksonFactory();
	private static GoogleAuthorizationCodeFlow flow = null;
	private static Drive service = null;
	
	//Registration Page - Request Authorisation Code from Google
	@RequestMapping(value="/register")
	public String register(Model uiModel)
	{  
		//This method is used to authorise the GoalKeeper to use the GKUsers google drive account.
		//The authorisation URL is generated and output.
	    flow = new GoogleAuthorizationCodeFlow.Builder(
	        httpTransport, jsonFactory, CLIENT_ID, CLIENT_SECRET, Arrays.asList(DriveScopes.DRIVE))
	        .setAccessType("online")
	        .setApprovalPrompt("auto").build();
	    
	    String url = flow.newAuthorizationUrl().setRedirectUri(REDIRECT_URI).build();
	    System.out.println("Request authorization code at:\n"+url);
	    
	    //Redirect to the url (authorisation code request url), the url goes to Google who will then return with an authorisation code at /register2 
	     
		return "redirect:" +url;
	}
	
	//Get Authorisation code from Google and exchange for token
	@RequestMapping(value="/register2", params={"code"})
	public String getCode(@RequestParam(value="code") String code, Model uiModel) 
	{
		System.out.println("authorisation code: "+code);
		
		//Use the returned authorisation code and send to Google to get token
		GoogleTokenResponse response = null;
		try {
			response = flow.newTokenRequest(code).setRedirectUri(REDIRECT_URI).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	    GoogleCredential credential = new GoogleCredential().setFromTokenResponse(response);
	    
	    //Create a new authorized API client
	    service = new Drive.Builder(httpTransport, jsonFactory, credential).build();
	    //service.
		return "register";
	}
	
	
	//Insert a File_______________________________________________________________________________________________
	//First View to get the file path of the document
	@RequestMapping(value="/insert")
	public String insert(Model uiModel)
	{
		return "insert";
	}
	
	//Second View to create the document in Google Drive
	@RequestMapping(value="/insert", method=RequestMethod.POST)
	public String insert(HttpServletRequest httpServletRequest)
	{
		String fileName = httpServletRequest.getParameter("fileName");
		System.out.println(httpServletRequest.getParameter("fileName"));
		//System.out.println(httpServletRequest.getAttribute("uploadfile"));
		
		
	    //Insert a file  
	    File body = new File();
	    body.setTitle(fileName);
	    body.setDescription("A test document");
	    body.setMimeType("application/vnd.google-apps.document");
	    
	   // java.io.File fileContent = new java.io.File("/Users/davidfarias/document2.txt");
	    java.io.File fileContent = new java.io.File("/Users/davidfarias/document2.txt");
	    FileContent mediaContent = new FileContent("text/plain", fileContent);

	    File file = null;
		try {
			file = service.files().insert(body, mediaContent).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    System.out.println("File ID: " + file.getId());
	    
		return "register";
	}
	
	//List all drive items__________________________________________________________________________________________
	@RequestMapping(value="/list")
	public String listing(Model uiModel)
	{
		System.out.println("In Listing method");
		
	    List<File> result = new ArrayList<File>();
	    Files.List request = null;
		try {
			request = service.files().list();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	    do {
	      try {
	        FileList files = request.execute();

	        result.addAll(files.getItems());
	        request.setPageToken(files.getNextPageToken());
	      } catch (IOException e) {
	        System.out.println("An error occurred: " + e);
	        request.setPageToken(null);
	      }
	    } while (request.getPageToken() != null &&
	             request.getPageToken().length() > 0);
	   
	    //Transfer contents of list to new List
	    List<DriveFile> listOfFiles = new ArrayList<DriveFile>();
	    
	    for(int i=0;i<result.size();i++)
	    {
	    	
	    	/*
	    	try
	    	{
	    	System.out.println("download link: "+result.get(i).getExportLinks().get("application/pdf"));
	    	System.out.println("File Id: "+result.get(i).getId());
	    	System.out.println("Title: "+result.get(i).getTitle());
	    	System.out.println("view link: "+result.get(i).getAlternateLink());
	    	System.out.println("download link: "+result.get(i).getExportLinks().get("application/pdf"));
	    	assert (result.get(i) != null);
	    	assert (result.get(i).getExportLinks()!= null);
	    	assert (result.get(i).getExportLinks().get("application/pdf")!= null);
	    	
	    	}
	    	catch (Exception e)
	    	{
	    		System.out.println("ERRORL " + e.toString());
	    		e.printStackTrace();
	    	}
	    	
	    	*/
	    	DriveFile df = new DriveFile(result.get(i).getId(),result.get(i).getTitle(), result.get(i).getAlternateLink(), result.get(i).getExportLinks().get("application/pdf"));
	    	df.setDownloadLinkTxt(result.get(i).getExportLinks().get("text/plain"));
	    	//df.getComments().add(result.get(i).g)
	   
	    	listOfFiles.add(df);
	    }
	 
	    uiModel.addAttribute("listResults", listOfFiles);
	    
		return "list";
		
	}
	
	//Analytics Method________________________________________________________________________________________
	
	
	@RequestMapping(value="/analytics/{id}", produces = "text/html")
	public String analytics(@PathVariable("id") String id, Model uiModel)
	//public String analytics(Model uiModel, String fileID)
	{
		System.out.println("In Analytics Method");
		File file = null;
		try {
			file = service.files().get(id).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Create custom DriveFile object and initialise
    	DriveFile df = new DriveFile(file.getId(),file.getTitle(), file.getAlternateLink(), file.getExportLinks().get("application/pdf"));
		df.setDownloadLinkTxt(file.getExportLinks().get("text/plain"));
		
		System.out.println(file.getLastModifyingUserName());
		System.out.println(file.getLastViewedByMeDate().toStringRfc3339());
		System.out.println(file.getLastViewedByMeDate().toString());
		System.out.println(file.getModifiedByMeDate());
		System.out.println(file.getModifiedDate());
		
		
		
		df.setLastUserToModify(file.getLastModifyingUserName());
		df.setLastTimeIViewed(file.getLastViewedByMeDate());
		df.setLastTimeIModified(file.getModifiedByMeDate());
		df.setLastModifiedDate(file.getModifiedDate());
		
		 printDriveFile(df);
			
		//Link to downloaded file
		String fileName = df.getName();
		fileName=fileName.replaceAll("\\s+",""); //remove whitespace from file name
		String pathToDownloadedFile = "/Users/davidfarias/Downloads/"+fileName+".txt";
		
		//Delete file if already there
		java.io.File fileTemp = new java.io.File(pathToDownloadedFile );
		if (fileTemp.exists())
		    fileTemp.delete();
		
		//Open new window to download file
				try {
					openWebpage(new URL(df.getDownloadLinkTxt()));
					Thread.sleep(9000);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		
    	Scanner input=null;
		try {
			input = new Scanner(new java.io.File(pathToDownloadedFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        int countWords = 0;
        //while there are more lines
        while (input.hasNextLine()) {
            //goes to each next word
            String word = input.next();
            //counts each word
            countWords++;
        }
 
    	df.setWordCount(countWords);
    	System.out.println("WordCount: "+countWords);
		
		
    	uiModel.addAttribute("driveFile", df);
		
		//return "redirect:" +df.getDownloadLink();
		return "analytics";
	}
	
	public static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}

	public static void openWebpage(URL url) {
	    try {
	        openWebpage(url.toURI());
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    }
	}
	
	public static void printDriveFile(DriveFile dfp)
	{
		System.out.println("Name: "+dfp.getName());
		System.out.println("Download Link: "+dfp.getDownloadLink());
		System.out.println("FileID: "+dfp.getFileID());
		System.out.println("Last User Who Modified: "+dfp.getLastUserToModify());
		System.out.println("View Link: "+dfp.getViewLink());
	}
	
}
