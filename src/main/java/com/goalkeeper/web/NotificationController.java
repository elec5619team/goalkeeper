package com.goalkeeper.web;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.goalkeeper.domain.Notification;
import com.goalkeeper.domain.Task;
import com.goalkeeper.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;

@RequestMapping("/notifications")
@Controller
@RooWebScaffold(path = "notifications", formBackingObject = Notification.class)
public class NotificationController {

	//Must be declared and passed through for @Autowired
    @Autowired
    private MailSender mailTemplate;
    private EmailService email;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Notification notification, BindingResult result, Model model, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("notification", notification);
            return "notifications/create";
        }
        //Must have a notification method
        if (!notification.getTwitter() && !notification.getEmail()) {
            model.addAttribute("notification", notification);
        	return "notification/create";
        }
        notification.persist();
        //Send an email if a notification is created
        if (notification.getEmail()) {
            try {
                email = new EmailService();
                email.sendMessage("gknotfiy@gmail.com", "Notifcation Created", notification.getGKUser().getEmail(), "Notfication Created for: " + notification.getNotificationTime(), mailTemplate);
            } catch (MailException ex) {
                System.err.println(ex.getMessage());
            }
        }
        return "redirect:/notifications/" + encodeUrlPathSegment(notification.getId().toString(), request);
    }
    
    //Overide for replace update field with response
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Notification notification, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, notification);
            return "notifications/update";
        }
        //When a notfication gets a response note the time
        if (notification.getResponded()) {
            Date date = new Date();
            notification.setNotificationTime(date);
            notification.setResponded(true);
        }
        uiModel.asMap().clear();
        notification.merge();
        return "redirect:/notifications/" + encodeUrlPathSegment(notification.getId().toString(), httpServletRequest);
    }
}
