package com.goalkeeper.web;

import java.util.List;

import com.google.api.client.util.DateTime;

public class DriveFile 
{
	private String name;
	private String viewLink;
	private String downloadLink;
	private String downloadLinkTxt;
	private int wordCount;
	private String fileID;
	private List<String> comments;
	private String lastUserToModify;
	private DateTime  lastTimeIViewed;
	private DateTime lastTimeIModified;
	private DateTime lastModifiedDate;
	
	public DriveFile(String fileID,String name, String viewLink, String downloadLink)
	{
		this.name=name;
		this.viewLink=viewLink;
		this.downloadLink=downloadLink;
		this.fileID=fileID;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getViewLink()
	{
		return viewLink;
	}
	
	public void setName(String name)
	{
		this.name=name;
	}
	
	public void setViewLink(String viewLink)
	{
		this.viewLink=viewLink;
	}

	public String getDownloadLink() 
	{
		return downloadLink;
	}

	public void setDownloadLink(String downloadLink)
	{
		this.downloadLink = downloadLink;
	}

	public int getWordCount() 
	{
		return wordCount;
	}

	public void setWordCount(int wordCount) 
	{
		this.wordCount = wordCount;
	}

	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	public String getDownloadLinkTxt() {
		return downloadLinkTxt;
	}

	public void setDownloadLinkTxt(String downloadLinkTxt) {
		this.downloadLinkTxt = downloadLinkTxt;
	}

	public List<String> getComments() {
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

	public String getLastUserToModify() {
		return lastUserToModify;
	}

	public void setLastUserToModify(String lastUserToModify) {
		this.lastUserToModify = lastUserToModify;
	}

	public DateTime getLastTimeIViewed() {
		return lastTimeIViewed;
	}

	public void setLastTimeIViewed(DateTime lastTimeIViewed) {
		this.lastTimeIViewed = lastTimeIViewed;
	}

	public DateTime getLastTimeIModified() {
		return lastTimeIModified;
	}

	public void setLastTimeIModified(DateTime lastTimeIModified) {
		this.lastTimeIModified = lastTimeIModified;
	}

	public DateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(DateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	

}
