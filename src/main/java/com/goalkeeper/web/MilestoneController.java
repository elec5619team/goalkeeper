package com.goalkeeper.web;
import com.googlecode.charts4j.*;

import static com.googlecode.charts4j.Color.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.Milestone;
import com.goalkeeper.domain.Task;
import com.goalkeeper.domain.UserGroup;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/milestones")
@Controller
@RooWebScaffold(path = "milestones", formBackingObject = Milestone.class)
public class MilestoneController {

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel, Principal principal) {
        Milestone m = Milestone.findMilestone(id);
        // Make sure milestone exists
        if (m != null) {
            uiModel.addAttribute("ownerofmilestone", Milestone.isOwnerOfMilestone(GKUser.getLoggedInUser(principal), m));
            uiModel.addAttribute("milestone", m);
            uiModel.addAttribute("usergroup", m.getUsergroup());
            
            List<Task> mTasks = new ArrayList<Task>();
            mTasks.addAll(m.getTasks());
            
            // Sort the tasks by creation due date
		     Collections.sort(mTasks, new Comparator<Task>() {
		    		
		            public int compare(Task o1, Task o2) {
		                if (o1.getCreatedon() == o2.getCreatedon()) return 0;
		                return o1.getCreatedon().after(o2.getCreatedon()) ? -1 : 1;
		            }
		        });
            
            uiModel.addAttribute("tasks", mTasks);
            
            // Show a piechart showing how many completed tasks there
            // are for this milestone
            Long completeCount = 0L, incompleteCount = 0L;
            for (Task mTask : mTasks)
            {
            	//System.out.println("Progrss: " + mTask.getProgress().toString());
            	if (mTask.getProgress() != null && mTask.getProgress() == 100)
            	{
            		completeCount += 1L;
            	}
            	else 
            	{
            		incompleteCount += 1L;
				}
            }
            
            Long total = completeCount + incompleteCount;
            Double propComplete = completeCount.doubleValue() / total.doubleValue() * 100.0d;
                       
            //System.out.println("c: " + completeCount.toString() + " ic:" + incompleteCount.toString());
            //System.out.println("prop: " + propComplete.toString());
            Slice s1 = Slice.newSlice( propComplete.intValue(), GREEN, "Complete");
            Slice s2 = Slice.newSlice( 100-propComplete.intValue(), RED, "Incomplete");

            uiModel.addAttribute("complete", completeCount);
            uiModel.addAttribute("incomplete", incompleteCount);
            
            PieChart chart = GCharts.newPieChart(s1, s2);
            chart.setTitle("Task completion", BLACK, 16);
            chart.setSize(500, 200);
            uiModel.addAttribute("chart", chart.toURLString()); 
            
            
        }
        uiModel.addAttribute("itemId", id);
        return "milestones/show";
    }
}
