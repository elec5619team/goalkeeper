package com.goalkeeper.web;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import com.goalkeeper.domain.GKUser;
import com.goalkeeper.service.TweetService;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/gkusers")
@Controller
@RooWebScaffold(path = "gkusers", formBackingObject = GKUser.class)
public class GKUserController {

    private TweetService tweet;

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid GKUser GKUser_, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, GKUser_);
            return "gkusers/create";
        }
        uiModel.asMap().clear();
        GKUser_.persist();
        tweet = new TweetService();
        if (!GKUser_.getTwitter().isEmpty()) {
            tweet.sendMessage("Created your GoalKeeper Account", GKUser_.getTwitter());
        }
        return "redirect:/gkusers/" + encodeUrlPathSegment(GKUser_.getId().toString(), httpServletRequest);
    }
}
