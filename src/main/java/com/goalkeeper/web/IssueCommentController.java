package com.goalkeeper.web;
import com.goalkeeper.domain.IssueComment;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/issuecomments")
@Controller
@RooWebScaffold(path = "issuecomments", formBackingObject = IssueComment.class)
public class IssueCommentController {
}
