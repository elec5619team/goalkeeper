package com.goalkeeper.web;
import com.goalkeeper.domain.UpdateMessage;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/updatemessages")
@Controller
@RooWebScaffold(path = "updatemessages", formBackingObject = UpdateMessage.class)
public class UpdateMessageController {
}
