package com.goalkeeper.web;

import com.googlecode.charts4j.*;

import java.security.Principal;
import java.sql.DataTruncation;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.Milestone;
import com.goalkeeper.domain.Task;
import com.goalkeeper.service.EmailService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/tasks")
@Controller
@RooWebScaffold(path = "tasks", formBackingObject = Task.class)
public class TaskController {

    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private MailSender mailTemplate;

    private EmailService email;
    
    @RequestMapping(value = "/progressunit")
    public String progressUnit(@RequestParam(value = "id", required = true) Long taskId, Model uiModel, Principal principal, HttpServletRequest httpServletRequest) {
    	
        String url = "redirect:/tasks/" + encodeUrlPathSegment(taskId.toString(), httpServletRequest);
        // User can only progress on tasks they own
        GKUser loggedInUser = GKUser.getLoggedInUser(principal);
        // Only for valid ids
        Task getTask = Task.findTask(taskId);
        System.out.println(taskId);
        if (getTask == null) {
            System.out.println("Task doesn't exist!");
            return url;
        }
        if (getTask.getOwner() != null && !getTask.getOwner().equals(loggedInUser)) {
            System.out.println("Logged in user does not own this task!");
            return url;
        }
        if (getTask.getProgress() != null && getTask.getProgress() == 100) {
            System.out.println("Task already complete");
            return url;
        }
        getTask.setVersion(0);
        // Null checking everywhere
        if (getTask.getProgress() == null) getTask.setProgress(1); else getTask.setProgress(getTask.getProgress() + 1);
        getTask.merge();
        System.out.println("Task progress increased to " + getTask.getProgress());
               
        return url;
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(@RequestParam(value = "mid", required = false) Long milestoneId, Model uiModel) {
        populateEditForm(uiModel, new Task());
        //System.out.println("Create for milestone: " + milestoneId);
        if (milestoneId != null) {
            Milestone taskForMilestone = Milestone.findMilestone(milestoneId);
            if (taskForMilestone != null) {
                uiModel.addAttribute("milestone", taskForMilestone);
            } else {
                System.out.println("Could not find milestone with id " + milestoneId);
            }
            uiModel.addAttribute("mid", milestoneId);
        }
        return "tasks/create";
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Task task, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
        if (bindingResult.hasErrors()) {
            // Owner field should be null
            populateEditForm(uiModel, task);
            System.out.println("ERROR: ");
            for (ObjectError oeb : bindingResult.getAllErrors()) {
                System.out.println(oeb.toString());
            }
            System.out.println("TASK: " + task.toString());
            return "tasks/create";
        }
        
        GKUser loggedInUser = GKUser.getLoggedInUser(principal);
        
        Milestone linkToMilestone = null;
        String midStr = httpServletRequest.getParameter("mid");
        Long milestoneid = null;
        
        if (midStr != null && !midStr.equals("")) {
            milestoneid = Long.parseLong(midStr);
            System.out.println("Creating task for milestone " + milestoneid.toString());
        }

        if (milestoneid != null) {
            linkToMilestone = Milestone.findMilestone(milestoneid);
            System.out.println("Found milestone " + linkToMilestone.getMilestonename());
            if (linkToMilestone != null) {
                task.setMilestone(linkToMilestone);                
            } else {
                System.out.println("POST-taskcreate: Could not find milestone with id " + milestoneid);
            }
        }
       
        // Fill in certain parts of the model
        task.setCreatedon(new Date());
        task.setOwner(loggedInUser);
        uiModel.asMap().clear();
        task.persist();
        
        /*
        if (linkToMilestone != null)
        {
            // Send email to other group members
            for (GKUser gku : linkToMilestone.getUsergroup().getUsers())
            {
                try {
                    email = new EmailService();
                    email.sendMessage("gknotfiy@gmail.com", 
                    		"New Task Created For Milestone "  + linkToMilestone.getMilestonename(),
                    		gku.getEmail(), 
                    		loggedInUser.getUsername() + " has created a new task for Milestone " + linkToMilestone.getMilestonename() + " called " + task.getTaskname(), 
                    		mailTemplate);
                } catch (MailException ex) {
                    System.err.println(ex.getMessage());
                }
            };
        }*/
        
        
        return "redirect:/tasks/" + encodeUrlPathSegment(task.getId().toString(), httpServletRequest);
    }
}
