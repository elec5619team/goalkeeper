package com.goalkeeper.web;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.Issue;
import com.goalkeeper.domain.Milestone;
import org.omg.CosNaming._BindingIteratorImplBase;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/issues")
@Controller
@RooWebScaffold(path = "issues", formBackingObject = Issue.class)
public class IssueController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Issue issue, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, issue);
            System.out.println("VALIDATION FAILED");
            System.out.println("ERROR: ");
            for (ObjectError oeb : bindingResult.getAllErrors()) {
                System.out.println(oeb.toString());
            }
            return "issues/create";
        }
        // Validate milestone id passed in
        String midp = httpServletRequest.getParameter("mid");
        if (midp.equals("")) {
            System.out.println("Empty milestone id parameter specified");
            return "issues/create";
        }
        Long mid = Long.parseLong(midp);
        Milestone milestone = Milestone.findMilestone(mid);
        if (milestone == null) {
            System.out.println("Milestone with given id could not be found!");
            return "issues/create";
        }
        GKUser loggedInUser = GKUser.getLoggedInUser(principal);
        if (!Milestone.isOwnerOfMilestone(loggedInUser, milestone)) {
            System.out.println("Logged in user is not an owner of this milestone!");
            return "issues/create";
        }
        // Automatically fill in some details
        issue.setDateraised(new Date());
        issue.setOwner(loggedInUser);
        issue.setMilestone(milestone);
        //issue.
        uiModel.asMap().clear();
        issue.persist();
        return "redirect:/issues/" + encodeUrlPathSegment(issue.getId().toString(), httpServletRequest);
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(@RequestParam(value = "mid", required = true) Long milestoneId, Model uiModel) {
        populateEditForm(uiModel, new Issue());
        List<String[]> dependencies = new ArrayList<String[]>();
        if (Milestone.countMilestones() == 0) {
            dependencies.add(new String[] { "milestone", "milestones" });
        }
        if (GKUser.countGKUsers() == 0) {
            dependencies.add(new String[] { "gkuser", "gkusers" });
        }
        uiModel.addAttribute("dependencies", dependencies);
        uiModel.addAttribute("mid", milestoneId);
        return "issues/create";
    }
}
