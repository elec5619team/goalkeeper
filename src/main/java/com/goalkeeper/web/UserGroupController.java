package com.goalkeeper.web;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.RollbackException;
import javax.validation.Valid;
import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.UserGroup;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/usergroups")
@Controller
@RooWebScaffold(path = "usergroups", formBackingObject = UserGroup.class)
public class UserGroupController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid UserGroup userGroup, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, userGroup);
            return "usergroups/create";
        }
        // Automatically logged in user (creator of this group) a member
        userGroup.getUsers().add(GKUser.getLoggedInUser(principal));
        uiModel.asMap().clear();
        userGroup.persist();
        return "redirect:/usergroups/" + encodeUrlPathSegment(userGroup.getId().toString(), httpServletRequest);
    }

    @RequestMapping(value = "/mygroups")
    public String myGroups(Model uiModel, Principal principal) {
        GKUser loggedinUser = GKUser.getLoggedInUser(principal);
        //List<UserGroup> myGroups =
        uiModel.addAttribute("username", loggedinUser.getUsername());
        uiModel.addAttribute("usergroups", loggedinUser.getGroups());
        return "usergroups/mygroups";
    }

    @RequestMapping(value = "/{id}/add", produces = "text/html")
    public String adduser(@PathVariable("id") Long id, Model uiModel, HttpServletRequest httpServletRequest, Principal principal) {
        String uname = httpServletRequest.getParameter("addusername");
        UserGroup ug = UserGroup.findUserGroup(id);
        String url = "redirect:/usergroups/" + encodeUrlPathSegment(id.toString(), httpServletRequest);
        if (ug == null) {
            System.out.println("User group doesn't exist!");
            return url;
        }
        if (uname == null || uname.equals("")) {
            System.out.println("No username given!");
            return url;
        }
        GKUser addUser = GKUser.findUserByName(uname);
        if (addUser == null) {
            System.out.println("User " + uname + " does not exist!");
            return url;
        }
        if (ug.getUsers().contains(addUser)) {
            System.out.println("User " + addUser.getUsername() + " is already in the group!");
            return url;
        }
        ug.getUsers().add(addUser);
        ug.merge();
        System.out.println("Successfully added " + addUser.getUsername());
        return url;
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel, Principal principal) {
        UserGroup ug = UserGroup.findUserGroup(id);
        uiModel.addAttribute("memberofgroup", UserGroup.isUserMemberOfGroup(GKUser.getLoggedInUser(principal), ug));
        uiModel.addAttribute("usergroup", ug);
        uiModel.addAttribute("itemId", id);
        return "usergroups/show";
    }
}
