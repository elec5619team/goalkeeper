package com.goalkeeper.web;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import java.security.Principal;

import com.goalkeeper.domain.Achievement;
import com.goalkeeper.domain.GKUser;
import com.goalkeeper.domain.Task;
import com.goalkeeper.domain.UserGroup;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping("/achievements")
@Controller
@RooWebScaffold(path = "achievements", formBackingObject = Achievement.class)
public class AchievementController {
	

	@RequestMapping(method = RequestMethod.GET, produces = "text/html")
	  public String data(Model model, Principal principal ) {
		
	      model.addAttribute("username", principal.getName()); //get logged in username
	      model.addAttribute("achievements", Achievement.findAllAchievements()); //pass in achievement list
	      GKUser test = GKUser.findUserByName(principal.getName());
	      model.addAttribute("role", test.getGKRole());
	      ArrayList<Achievement> userA = new ArrayList<Achievement>();
	      userA = parseAchievements(principal.getName());
	      model.addAttribute("userAchievements", userA);
	      model.addAttribute("size1", userA.size());
	      model.addAttribute("size2", Achievement.findAllAchievements().size());
	      return "achievements/list"; //return view
	}

	
	public ArrayList<Achievement> parseAchievements(String username) {
	
		//setup
		GKUser u = GKUser.findUserByName(username);
		//List<Task> tasks = new ArrayList<Task>();
		//tasks = (Task.findAllTasks()); //get all tasks
		
		List<Task> myTasks = new ArrayList<Task>();
		//myTasks.addAll(u.getTasks());
		
		ArrayList<Achievement> achievements = new ArrayList<Achievement>();
		
		
		ArrayList<Achievement> listOfAchievements = new ArrayList<Achievement>();
		listOfAchievements = (ArrayList<Achievement>) Achievement.findAllAchievements();
		/* Achievement validation goes here */
			
		
		//has at least one total task
		if (u.getTasks().size() >= 1) {
			achievements.add(listOfAchievements.get(0));
		}
		
		//has at least ten tasks
		if (myTasks.size() >= 10) {
			achievements.add(listOfAchievements.get(1));
		}
		
		//has at least twenty five tasks
		if (myTasks.size() >= 25) {
			achievements.add(listOfAchievements.get(2));

		}
		
		return achievements;
		
	}
		/* End achievement validation */	

}
