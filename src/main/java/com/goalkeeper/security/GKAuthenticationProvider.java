package com.goalkeeper.security;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.hibernate.NonUniqueResultException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.goalkeeper.domain.GKUser;

public class GKAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider{
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, 
			UsernamePasswordAuthenticationToken authentication)
		throws AuthenticationException
	{
		// STUB
	}
	
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
	{
		String password = (String) authentication.getCredentials();
		if (!StringUtils.hasText(password))
		{
			throw new BadCredentialsException("Please enter a password");
		}
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		try {
			GKUser gkUser = GKUser.findGKUSerByUsernameAndPasswordEquals(username, password);
			authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
		}
		catch (EmptyResultDataAccessException e) {
			throw new BadCredentialsException("Invalid username or password");
		}
		catch (EntityNotFoundException e) {
			throw new BadCredentialsException("Invalid user");
		}
		catch (NonUniqueResultException e) {
			throw new BadCredentialsException("Non-unique user, uh oh");
		}
		return new User(username, password, 
				true, // enabled
				true, //accountNonExpired
				true, //credentialsNonExpired
				true, //accountNonLocked 
				authorities);
	}
}
